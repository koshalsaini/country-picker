//
//  ViewController.m
//  Location
//
//  Created by Click Labs 105 on 10/26/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
@import GoogleMaps;

@interface ViewController (){
    CLLocationManager *manager;
    CLLocation *currentLocation;
    NSString *city;
    GMSMapView *mapView;
    GMSMarker *marker;
    NSString *zipCode;
    NSString *state;
    NSString *zip;
    CLLocationCoordinate2D position;
    NSString *URL;
    NSString *address;
}

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;
@end

@implementation ViewController
@synthesize mapView;
@synthesize searchCity;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    [self Location];
    mapView.delegate = self;
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager startUpdatingLocation];
   
    
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//    
//                                       initWithTarget:self
//    
//                                       action:@selector(dismissTable)];
//    
//    [mapView addGestureRecognizer:tap];
//     CLLocationCoordinate2D position = CLLocationCoordinate2DMake(30.7500, 76.7800);
//    marker = [GMSMarker markerWithPosition:position];
//    marker.title = @"Welcome Chandigarh";
//    marker.map = mapView;
//    marker.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
//    marker.opacity = 0.6;
    
    self.appleMap.delegate = self;
    self.mapView.delegate = self;
    searchCity.delegate = self;
        [manager requestWhenInUseAuthorization];
}

- (IBAction)getLocation:(id)sender {
    [ manager startUpdatingLocation];
    //CLLocation *currentLocation=self.newLocation.placemark;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        MKPointAnnotation *Point = [[MKPointAnnotation alloc] init];
         position = currentLocation.coordinate;
        //MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 500, 500);
        //[self.appleMap setRegion:region animated:YES];
        Point.coordinate =  currentLocation.coordinate;
        Point.title = [NSString stringWithFormat:@"%@",city];
        Point.subtitle = @"sir";
        [_appleMap addAnnotation:Point];
        
        MKMapCamera *newCamera = [MKMapCamera cameraLookingAtCenterCoordinate:currentLocation.coordinate
                                                            fromEyeCoordinate:currentLocation.coordinate
                                                                  eyeAltitude:50];
        [newCamera setHeading:100.0]; // or newCamera.heading + 90.0 % 360.0
        [newCamera setPitch:45.0];
        //[newCamera setHeading:90.0];
        // [newCamera setAltitude:500.0];
        [_appleMap setCamera:newCamera animated:YES];
        
        [self.appleMap addAnnotation:Point];

       
        
//        GMSMarker *show = [[GMSMarker alloc] init];
//        show.position = currentLocation.coordinate;
//        show.title = [NSString stringWithFormat:@"%@",city];
//        show.snippet = @"";
//        show.map = _appleMap;
        
        
        [self Location];
    }
}
- (void)Location{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                        longitude:currentLocation.coordinate.longitude];
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           NSDictionary *addressDictionary = placemark.addressDictionary;
                           NSLog(@"%@ ", addressDictionary);
                           city = addressDictionary[@"City"];
                           NSLog(@"%@", city);
                           
                           
//                           address = [addressDictionary
//                                                objectForKey:(NSString *)kABPersonAddressStreetKey];
//                           city = [addressDictionary
//                                             objectForKey:(NSString *)kABPersonAddressCityKey];
//                           state = [addressDictionary
//                                              objectForKey:(NSString *)kABPersonAddressStateKey];
//                           zip = [addressDictionary
//                                            objectForKey:(NSString *)kABPersonAddressZIPKey];
//                           state = addressDictionary[@"state"];
//                           zip = addressDictionary[@"zip"];
//                           NSLog(@"%@", state);
//                           NSLog(@"%@", zip);
                       }
                   }];
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    position = coordinate;
    marker = [GMSMarker markerWithPosition:position];
    //show.position = currentLocation.coordinate;
//    marker.title = [NSString stringWithFormat:@"%@",city];
//    marker.snippet = @"";
//    marker.map = mapView;
    
    
    [self CilkAndMark];
}


-(void) CilkAndMark  {
    CLGeocoder *geocoder2 = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation2 = [[CLLocation alloc]initWithLatitude:position.latitude
                                                         longitude:position.longitude];
    
    [geocoder2 reverseGeocodeLocation:newLocation2
                    completionHandler:^(NSArray *placemarks, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Geocode failed with error: %@", error);
                            return;
                        }
                        
                        if (placemarks && placemarks.count > 0)
                        {
                            CLPlacemark *placemark = placemarks[0];
                            
                            NSDictionary *addressDictionary =
                            placemark.addressDictionary;
                            
                            NSLog(@"%@ ", addressDictionary);
                            if (addressDictionary[@"City"] != NULL ){
                                city = addressDictionary[@"City"];
                                NSLog(@"%@",city);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",city];
                                marker.map = mapView;
                            }
                            else if ((addressDictionary[@"State"] != NULL )){
                                state = addressDictionary[@"State"];
                                NSLog(@"%@",state);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",state];
                                marker.map = mapView;
                            }
                            
                            else if ((addressDictionary[@"Country"] != NULL )){
                                city = addressDictionary[@"city"];
                                NSLog(@"%@",city);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",city];
                                marker.map = mapView;
                                
                            }
                        }
                        
                        
                    }];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    address = searchCity.text;
    URL = [NSString
           stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyBhCly2RqKlh8c0-UmrpIPzTwsXpu33ojo",address];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *searchAddress = json[@"results"];
    for (int i=0; i<searchAddress.count; i++) {
        NSDictionary *searchFound = searchAddress[i];
        if ((searchFound[@"geometry"] != NULL)&&(searchFound[@"formatted_address"] != NULL)) {
            NSDictionary *Geometry = searchFound[@"geometry"];
            NSDictionary *location = Geometry[@"location"];
            NSString *lat = location[@"lat"];
            NSString *lng = location[@"lng"];
            address = searchFound[@"formatted_address"];
            position = CLLocationCoordinate2DMake(lat.doubleValue,lng.doubleValue);
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
                                                                    longitude:position.longitude
                                                                         zoom:10.0];
            [mapView animateToCameraPosition:camera];
            marker = [GMSMarker markerWithPosition:position];
            marker.title = [NSString stringWithFormat:@"%@",address];
            marker.map = mapView;
            
            
            
            
        }
        
    }
    
}
- (void) didPan:(UIPanGestureRecognizer*) gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        mapView.settings.scrollGestures = true;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (gestureRecognizer.numberOfTouches > 1)
    {
        mapView.settings.scrollGestures = false;
    }
    else
    {
        mapView.settings.scrollGestures = true;
    }
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
