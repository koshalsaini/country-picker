//
//  ViewController.h
//  Location
//
//  Created by Click Labs 105 on 10/26/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,GMSMapViewDelegate,UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;
@end

